# mini-project3: Create an S3 Bucket using CDK with AWS CodeWhisperer
> Xingyu, Zhang (NetID: xz365)

## Setting up the CDK Project
1. __Install the AWS CDK CLI__: 
```bash
npm install -g aws-cdk
```

2. __Create a new CDK project__: 
```bash
mkdir cdkproject
cd cdkproject
cdk init app --language typescript
```
These commands will create a new CDK project written in ```typescript``` in a new directory. 

3. __Add the S3 package__: Include the AWS S3 package to the project.
```bash
npm install @aws-cdk/aws-s3
```

## Getting Started with CodeWhisperer
CodeWhisperer can be integrated with popular IDEs such as _Visual Studio Code, JetBrains IDEs (like IntelliJ IDEA and PyCharm), and AWS Cloud9_. 

For __VSCODE__, install the extension named ```AWS Toolkit - Amazon Q, CodeWhisperer, and more``` from VSCODE’s Extensions and authenticate it using your AWS account credentials.

More information about [CodeWhisperer](https://aws.amazon.com/cn/pm/codewhisperer/?gclid=CjwKCAiArLyuBhA7EiwA-qo80P0w1RKcmyb3gCFQgwR7_8yyipNu7sRze0dzcQeV52A_YBk1GWg0dhoCdtUQAvD_BwE&trk=1e9faa86-7e39-47e1-a37e-ed70cd5100c0&sc_channel=ps&ef_id=CjwKCAiArLyuBhA7EiwA-qo80P0w1RKcmyb3gCFQgwR7_8yyipNu7sRze0dzcQeV52A_YBk1GWg0dhoCdtUQAvD_BwE:G:s&s_kwcid=AL!4422!3!662403130556!!!g!!!20274325379!148903075326).

## Using CodeWhisperer for AWS CDK Development
1. Open the stack file within the lib directory.
   
2. Write the comment that describes the functionality we want to implement. In the case of creating an S3 bucket with versioning and encryption enabled:(the suggestion will displayed after ```ENTER```)
```ts
// Import the S3 module
```
```js
// Create an S3 bucket with versioning enabled and AES-256 encryption
```

3. CodeWhisperer might offer suggestions that automatically complete the code. Accept these inline suggestions with a keyboard shortcut, typically ```Tab```. The suggestions generated for previous two comments are:
```js
// Import the S3 module
import * as s3 from 'aws-cdk-lib/aws-s3';
```
```js
// Define an S3 bucket with versioning enabled and server-side encryption
    const bucket = new s3.Bucket(this, 'XXXXXXXXXXX', {
      versioned: true,
      encryption: s3.BucketEncryption.S3_MANAGED,
    }
```

4. Modify and extend the code as needed after inserting a suggestion. In the second piece of code in step3, we need to change ```'XXXXXXXXXXX'``` to our ```Bucket name```.

### The completed code for the bucket
```js
import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

// Import the S3 module
import * as s3 from 'aws-cdk-lib/aws-s3';

export class CdkProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    // Define an S3 bucket with versioning enabled and server-side encryption
    const bucket = new s3.Bucket(this, '721s3bucket', {
      versioned: true,
      encryption: s3.BucketEncryption.S3_MANAGED,
    })
  }
}
```



## Deploying the Stack
1. Bootstrap the AWS environment: 
```bash
cdk bootstrap
```
```bash
 ⏳  Bootstrapping environment aws://211125444211/us-east-1...
Trusted accounts for deployment: (none)
Trusted accounts for lookup: (none)
Using default execution policy of 'arn:aws:iam::aws:policy/AdministratorAccess'. Pass '--cloudformation-execution-policies' to customize.
CDKToolkit: creating CloudFormation changeset...
CDKToolkit |  0/12 | 00:30:50 | REVIEW_IN_PROGRESS   | AWS::CloudFormation::Stack | CDKToolkit User Initiated
CDKToolkit |  0/12 | 00:30:56 | CREATE_IN_PROGRESS   | AWS::CloudFormation::Stack | CDKToolkit User Initiated
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | CloudFormationExecutionRole
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::S3::Bucket         | StagingBucket
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::ECR::Repository    | ContainerAssetsRepository
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | ImagePublishingRole
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | FilePublishingRole
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::SSM::Parameter     | CdkBootstrapVersion
CDKToolkit |  0/12 | 00:30:59 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | LookupRole
CDKToolkit |  0/12 | 00:31:00 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | LookupRole Resource creation Initiated
CDKToolkit |  0/12 | 00:31:00 | CREATE_IN_PROGRESS   | AWS::SSM::Parameter     | CdkBootstrapVersion Resource creation Initiated
CDKToolkit |  0/12 | 00:31:00 | CREATE_IN_PROGRESS   | AWS::ECR::Repository    | ContainerAssetsRepository Resource creation Initiated
CDKToolkit |  0/12 | 00:31:00 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | ImagePublishingRole Resource creation Initiated
CDKToolkit |  1/12 | 00:31:01 | CREATE_COMPLETE      | AWS::ECR::Repository    | ContainerAssetsRepository
CDKToolkit |  2/12 | 00:31:01 | CREATE_COMPLETE      | AWS::SSM::Parameter     | CdkBootstrapVersion
CDKToolkit |  2/12 | 00:31:01 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | FilePublishingRole Resource creation Initiated
CDKToolkit |  2/12 | 00:31:01 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | CloudFormationExecutionRole Resource creation Initiated
CDKToolkit |  2/12 | 00:31:03 | CREATE_IN_PROGRESS   | AWS::S3::Bucket         | StagingBucket Resource creation Initiated
CDKToolkit |  3/12 | 00:31:18 | CREATE_COMPLETE      | AWS::IAM::Role          | ImagePublishingRole
CDKToolkit |  4/12 | 00:31:18 | CREATE_COMPLETE      | AWS::IAM::Role          | LookupRole
CDKToolkit |  5/12 | 00:31:18 | CREATE_COMPLETE      | AWS::IAM::Role          | FilePublishingRole
CDKToolkit |  6/12 | 00:31:19 | CREATE_COMPLETE      | AWS::IAM::Role          | CloudFormationExecutionRole
CDKToolkit |  6/12 | 00:31:19 | CREATE_IN_PROGRESS   | AWS::IAM::Policy        | ImagePublishingRoleDefaultPolicy
CDKToolkit |  6/12 | 00:31:20 | CREATE_IN_PROGRESS   | AWS::IAM::Policy        | ImagePublishingRoleDefaultPolicy Resource creation Initiated
CDKToolkit |  7/12 | 00:31:30 | CREATE_COMPLETE      | AWS::S3::Bucket         | StagingBucket
CDKToolkit |  7/12 | 00:31:33 | CREATE_IN_PROGRESS   | AWS::IAM::Policy        | FilePublishingRoleDefaultPolicy
CDKToolkit |  7/12 | 00:31:33 | CREATE_IN_PROGRESS   | AWS::S3::BucketPolicy   | StagingBucketPolicy
CDKToolkit |  7/12 | 00:31:33 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | DeploymentActionRole
CDKToolkit |  7/12 | 00:31:34 | CREATE_IN_PROGRESS   | AWS::IAM::Policy        | FilePublishingRoleDefaultPolicy Resource creation Initiated
CDKToolkit |  7/12 | 00:31:34 | CREATE_IN_PROGRESS   | AWS::S3::BucketPolicy   | StagingBucketPolicy Resource creation Initiated
CDKToolkit |  7/12 | 00:31:34 | CREATE_IN_PROGRESS   | AWS::IAM::Role          | DeploymentActionRole Resource creation Initiated
CDKToolkit |  8/12 | 00:31:35 | CREATE_COMPLETE      | AWS::S3::BucketPolicy   | StagingBucketPolicy
CDKToolkit |  9/12 | 00:31:36 | CREATE_COMPLETE      | AWS::IAM::Policy        | ImagePublishingRoleDefaultPolicy
CDKToolkit | 10/12 | 00:31:50 | CREATE_COMPLETE      | AWS::IAM::Policy        | FilePublishingRoleDefaultPolicy
CDKToolkit | 11/12 | 00:31:52 | CREATE_COMPLETE      | AWS::IAM::Role          | DeploymentActionRole
CDKToolkit | 12/12 | 00:31:55 | CREATE_COMPLETE      | AWS::CloudFormation::Stack | CDKToolkit
 ✅  Environment aws://211125444211/us-east-1 bootstrapped.
```
2. Deploy the stack to AWS:
```bash
cdk deploy
```
```bash
✨  Synthesis time: 3.59s

current credentials could not be used to assume 'arn:aws:iam::211125444211:role/cdk-hnb659fds-deploy-role-211125444211-us-east-1', but are for the right account. Proceeding anyway.
CdkProjectStack:  start: Building 381479ff8c29443de8719e18b3acc630f2d56311d6d3e573dfb8c993beddf55c:current_account-current_region
CdkProjectStack:  success: Built 381479ff8c29443de8719e18b3acc630f2d56311d6d3e573dfb8c993beddf55c:current_account-current_region
CdkProjectStack:  start: Publishing 381479ff8c29443de8719e18b3acc630f2d56311d6d3e573dfb8c993beddf55c:current_account-current_region
current credentials could not be used to assume 'arn:aws:iam::211125444211:role/cdk-hnb659fds-file-publishing-role-211125444211-us-east-1', but are for the right account. Proceeding anyway.
CdkProjectStack:  success: Published 381479ff8c29443de8719e18b3acc630f2d56311d6d3e573dfb8c993beddf55c:current_account-current_region
current credentials could not be used to assume 'arn:aws:iam::211125444211:role/cdk-hnb659fds-lookup-role-211125444211-us-east-1', but are for the right account. Proceeding anyway.
(To get rid of this warning, please upgrade to bootstrap version >= 8)
current credentials could not be used to assume 'arn:aws:iam::211125444211:role/cdk-hnb659fds-deploy-role-211125444211-us-east-1', but are for the right account. Proceeding anyway.
CdkProjectStack: deploying... [1/1]
CdkProjectStack: creating CloudFormation changeset...
CdkProjectStack | 0/3 | 00:41:39 | REVIEW_IN_PROGRESS   | AWS::CloudFormation::Stack | CdkProjectStack User Initiated
CdkProjectStack | 0/3 | 00:41:45 | CREATE_IN_PROGRESS   | AWS::CloudFormation::Stack | CdkProjectStack User Initiated
CdkProjectStack | 0/3 | 00:41:48 | CREATE_IN_PROGRESS   | AWS::S3::Bucket    | 721s3bucket (721s3bucket829EF2AF)
CdkProjectStack | 0/3 | 00:41:48 | CREATE_IN_PROGRESS   | AWS::CDK::Metadata | CDKMetadata/Default (CDKMetadata)
CdkProjectStack | 0/3 | 00:41:49 | CREATE_IN_PROGRESS   | AWS::CDK::Metadata | CDKMetadata/Default (CDKMetadata) Resource creation Initiated
CdkProjectStack | 1/3 | 00:41:49 | CREATE_COMPLETE      | AWS::CDK::Metadata | CDKMetadata/Default (CDKMetadata)
CdkProjectStack | 1/3 | 00:41:51 | CREATE_IN_PROGRESS   | AWS::S3::Bucket    | 721s3bucket (721s3bucket829EF2AF) Resource creation Initiated
CdkProjectStack | 2/3 | 00:42:16 | CREATE_COMPLETE      | AWS::S3::Bucket    | 721s3bucket (721s3bucket829EF2AF)
CdkProjectStack | 3/3 | 00:42:18 | CREATE_COMPLETE      | AWS::CloudFormation::Stack | CdkProjectStack

 ✅  CdkProjectStack

✨  Deployment time: 44.33s

Stack ARN:
arn:aws:cloudformation:us-east-1:211125444211:stack/CdkProjectStack/0e6ab2c0-cc8e-11ee-8513-0e5fe0368fe9

✨  Total time: 47.92s
```

## Finding the created S3 bucket in the AWS Management Console
The deployed S3 bucket can be found in [Amazon S3 > Buckets
](https://s3.console.aws.amazon.com/s3/buckets?region=us-east-1&bucketType=general) in the AWS Management Console.

### Screenshot of the bucket's properties
![bucket info](./cdk-shortcut.png)